module.exports = {
  purge: {
    enabled: process.env.NODE_ENV === "production" ? true : false,
    content: ["./src/**/*.{js,jsx,ts,tsx}", "./public/index.html"],
  },
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      margin: {
        "top-100": "100%",
        "left-100": "100%",
      },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
};
