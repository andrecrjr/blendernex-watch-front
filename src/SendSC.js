import * as waxjs from "@waxio/waxjs/dist";

const wax = new waxjs.WaxJS("https://wax.greymass.com", null, null, false);

const transformAssets = async (user, memo, asset_ids = []) => {
  const trans = await wax.api.transact({
    account: "atomicassets",
    name: "transfer",
    authorization: [
      {
        actor: user,
        permission: "active"
      }
    ],
    data: {
      asset_ids,
      from: user,
      memo: memo,
      to: "blenderizerx"
    }
  });
  return;
};
