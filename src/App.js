import React from "react";
import Main from "./routes/Main";
import CollectionPage from "./routes/Collection";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Suggestion from "./routes/Suggestion";
import * as ReactGA from "react-ga";

import Donation from "./routes/Donation";
export default function App() {
  ReactGA.initialize(
    window.location.origin !== "https://blenderizer-watch.surge.sh"
      ? "UA-188021889-1"
      : process.env.REACT_APP_GA_CODE,
    {
      debug:
        window.location.origin !== "https://blenderizer-watch.surge.sh"
          ? true
          : false,
    }
  );

  return (
    <Router>
      <Switch>
        <Route exact path='/'>
          <Main />
        </Route>
        <Route path='/collection'>
          <CollectionPage />
        </Route>
        <Route exact path='/collection/:blendercollection/:page'>
          <CollectionPage />
        </Route>
        <Route exact path='/suggestion'>
          <Suggestion />
        </Route>
        <Route exact path='/donation'>
          <Donation />
        </Route>
      </Switch>
    </Router>
  );
}
