import React, { useEffect } from "react";
import Layout from "../components/Layout";
import { useGA } from "../components/hooks/useGA";

function Suggestion(props) {
  const { setPageview } = useGA();

  useEffect(() => {
    setPageview();
  }, [setPageview]);
  return (
    <Layout>
      <section className='flex flex-col justify-center items-center mt-16'>
        <h1 className='text-white font-bold'>
          I'm a front-end developer that likes blockhain!
        </h1>
        <p className='text-white  text-xs'>
          If you want to help suggesting something to improve, use the form
          bellow!
        </p>
        <p className='text-white text-xs'>
          ps: The Blenderizer was not created by me, i'm just a front-end
          blockchain lover!
        </p>
        <iframe
          src='https://docs.google.com/forms/d/e/1FAIpQLSegdt6uKs67rqj2MyY4P46sO7x-G47fOAHb2jVOX5Haqo8ZBw/viewform?embedded=true'
          width={window.outerWidth}
          height='803'
          frameborder='0'
          marginheight='0'
          marginwidth='0'
          title='Suggestion frame'
        >
          Loading...
        </iframe>
      </section>
    </Layout>
  );
}

export default Suggestion;
