import React from "react";
import MainMixture from "../components/MixtureCollection";
import { useRouteMatch, Switch, Route } from "react-router-dom";
import Layout from "../components/Layout";
import { MemoMixture } from "../components/MixtureCollection/MemoMixture";

const CollectionPage = () => {
  let { path } = useRouteMatch();
  return (
    <Switch>
      <Route exact path={`${path}/:blendercollection`}>
        <Layout>
          <MainMixture />
        </Layout>
      </Route>
      <Route exact path={`${path}/iframe/:blendercollection`}>
        <MainMixture iframe={true} />
      </Route>
      <Route exact path={`${path}/memo/:memoCard`}>
        <Layout>
          <MemoMixture />
        </Layout>
      </Route>
    </Switch>
  );
};

export default CollectionPage;
