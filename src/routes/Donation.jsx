import React, { useEffect } from "react";
import Layout from "../components/Layout";
import { useGA } from "../components/hooks/useGA";
function Donation(props) {
  const { setPageview } = useGA();
  useEffect(() => {
    setPageview();
  }, [setPageview]);
  return (
    <Layout>
      <section className='mt-20'>
        <p>
          Donating is the better way to help me with the evolution of this
          WebApp, you can donate any amount in some of cryptocurrencies bellow:
          If you have another one
        </p>
        <section className='flex flex-col sm:flex-row sm:justify-between mx-4 flex-wrap'>
          <div className='donate--gallery'>
            <h1 className='font-bold text-xl text-white'>WAX</h1>
            <p>rd2aw.wam</p>
            <img
              src='/assets/wax_qr.png'
              className='pt-4'
              width='150'
              alt='wax qr code'
            />
          </div>
          <div className='donate--gallery'>
            <h1 className='font-bold text-xl text-white'>BANANO</h1>
            <p>
              ban_1eroshi3kz1ye9o6c6nxqu5zzfhxmc9mqugg9uf8nfk1nw5nnx6q5r66e3ke
            </p>
            <img
              src='/assets/banano_qr.png'
              className='pt-4'
              width='150'
              alt='banano qr code'
            />
          </div>
          <div className='donate--gallery'>
            <h1 className='font-bold text-xl text-white'>NANO</h1>
            <p>
              nano_1eroshi3kz1ye9o6c6nxqu5zzfhxmc9mqugg9uf8nfk1nw5nnx6q5r66e3ke
            </p>
            <img
              src='/assets/nano_qr.png'
              className='pt-4'
              width='150'
              alt='nano qr code'
            />
          </div>
        </section>
      </section>
    </Layout>
  );
}

export default Donation;
