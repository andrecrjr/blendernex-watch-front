import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import axios from "axios";
import Layout from "../components/Layout";

import useBlenderContract from "../components/hooks/useBlenderContract";
import { useGA } from "../components/hooks/useGA";

function Main(props) {
  const [data, getCollection] = useBlenderContract("", "INITIAL_PAGE");
  const { setPageview } = useGA();
  useEffect(() => {
    getCollection();
  }, [getCollection]);

  useEffect(() => {
    setPageview();
  }, [setPageview]);

  return (
    <Layout>
      <section className='w-full block'>
        <h1 className='text-center text-white font-bold text-xl pb-3'>
          Collections
        </h1>
        <ul className='flex flex-wrap sm:items-center sm:mx-22 sm:justify-center'>
          {data.length > 0
            ? data.map((collections, index) => {
                if (collections.collection === "testtestte12") return null;
                if (collections.collection === "fakeingheroz") return null;
                return (
                  <li className='contents' key={index}>
                    <Collections collection={collections.collection} />
                  </li>
                );
              })
            : null}
        </ul>
      </section>
    </Layout>
  );
}

function Collections({ collection }) {
  const [collectionInfo, setInfo] = useState();
  useEffect(() => {
    const getUser = async () => {
      const { data } = await axios.get(
        `https://wax.api.atomicassets.io/atomicassets/v1/collections/${collection}`
      );
      setInfo(data.data);
    };
    getUser();
  }, [collection]);

  return (
    <div
      className='w-6/12 sm:w-3/12 md:mx-4 
    flex flex-col justify-center sm:flex-row items-center pb-2 pt-2'
    >
      <Link
        to={`/collection/${collection}`}
        className='text-sm sm:pr-2 sm:pl-2'
      >
        <p className='text-white bold'>{collection}</p>
      </Link>
      <Link to={`/collection/${collection}`} className=''>
        <img
          src={`https://ipfs.io/ipfs/${collectionInfo?.img || ""}`}
          className='rounded w-12 pt-2 sm:pt-0 sm:w-14'
          alt={`${collection}`}
        />
      </Link>
    </div>
  );
}

export default Main;
