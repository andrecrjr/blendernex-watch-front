import React, { useEffect, useState, useCallback, useContext } from "react";
import axios from "axios";
import { MixtureContext } from "../components/contexts";
import { Link } from "react-router-dom";
import { ReactComponent as Next } from "../assets/next.svg";

export const Card = ({ id, target, collection }) => {
  const [card, setCard] = useState({ immutable_data: { img: "" } });
  const [cardTarget, setTarget] = useState({ immutable_data: { img: "" } });
  const { mixtureState, dispatchMixtureReducer } = useContext(MixtureContext);

  const getAsset = useCallback(
    async (newId, setData) => {
      const { data } = await axios.get(
        `https://wax.api.atomicassets.io/atomicassets/v1/templates/${collection}/${newId}`
      );
      if (target) {
        dispatchMixtureReducer({
          type: "GET_MIXTURE_TARGET",
          payload: data.data,
        });
      }
      setData(data.data);
    },
    [collection, target, dispatchMixtureReducer]
  );

  useEffect(() => {
    if (id) getAsset(id, setCard);
  }, [id, getAsset]);

  useEffect(() => {
    if (target) getAsset(target, setTarget);
  }, [target, getAsset]);

  if (id) {
    return (
      <div className='flex relative w-24'>
        {card.immutable_data.img !== "" ? (
          <CardImage collection={collection} card={card} />
        ) : (
          <>
            <img src='/assets/loading.gif' alt='loading' width='100' />
          </>
        )}
      </div>
    );
  }
  if (target) {
    return (
      <div className='relative w-36 sm:pl-5 flex flex-col mt-8 sm:mt-0 self-center'>
        {mixtureState.card_done || mixtureState.is_infinite_supply ? (
          <>
            <h1 className='text-white text-xs font-bold'>Memo: {target}</h1>
          </>
        ) : (
          <p className='text-white font-bold'>Finished! :(</p>
        )}
        <div className='relative'>
          {cardTarget.immutable_data.img !== "" ? (
            <CardImage
              collection={collection}
              card={cardTarget}
              target={{ target, isTarget: true }}
            />
          ) : (
            <>
              <img src='/assets/loading.gif' alt='loading' width='100' />
            </>
          )}
        </div>
      </div>
    );
  }

  return null;
};

export const CardImage = ({ collection, card, target }) => {
  return (
    <>
      <div className='contents'>
        <a
          href={`https://wax.atomichub.io/explorer/template/${collection}/${card.template_id}`}
          className='contents'
          target='_blank'
          rel='noopener noreferrer'
        >
          <img
            src={`https://ipfs.io/ipfs/${card.immutable_data.img}`}
            alt={`${collection}`}
            className='transition-all duration-500'
          />

          <div
            className={`absolute bottom-0 justify-between items-end flex opacity-70 w-full`}
          >
            {target?.isTarget && (
              <Link
                to={`/collection/memo/${target.target}`}
                className='bg-white p-1 rounded-sm'
                target='_blank'
              >
                <span>
                  <Next width='20px' />
                </span>
              </Link>
            )}
            <p
              className={`text-xs rounded-sm p-1 font-bold bg-white self-end hover:bg-gray-200`}
              style={{
                color: "black",
              }}
            >
              {`${card.issued_supply}/${
                parseInt(card.max_supply) === 0 ? `∞` : card.max_supply
              }`}
            </p>
          </div>
        </a>
      </div>
    </>
  );
};
