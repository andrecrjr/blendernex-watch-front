import React from "react";
import { Link } from "react-router-dom";
import { ReactComponent as Menu } from "../../assets/menu.svg";
import { ReactComponent as TelegramIcon } from "../../assets/icons/telegram.svg";
import { ReactComponent as TwitterIcon } from "../../assets/icons/twt.svg";

function Layout({ children }) {
  return (
    <section>
      <header className='w-full h-11 flex items-center justify-between mb-5 z-20 top-0 bg-black fixed'>
        <div></div>
        <Link to='/'>
          <h1 className='text-white text-xl font-bold'>Blenderizer Watch</h1>
        </Link>
        <label htmlFor='menu' className='z-20 pr-4 md:hidden'>
          <Menu fill='white' className='cursor-pointer' />
        </label>
        <input type='checkbox' id='menu' className='h-0 contents' />
        <ul className='menu'>
          <li className='menu--option font-bold'>
            <Link to='/donation'>Donations</Link>
          </li>
          <li className='menu--option'>
            <Link to='/suggestion'>Suggestions!</Link>
          </li>
          <li className='menu--option'>
            <a
              href='https://3dkrender.com/blenderizer-move-your-assets/'
              target='_blank'
              rel='noopener noreferrer'
            >
              Blenderizer?
            </a>
          </li>
          <li className='mt-top-100 flex pl-4 md:pl-0 md:fixed md:bottom-3 md:right-3  text-white right-0'>
            <a
              href='https://t.me/eroshi'
              target='_blank'
              title='Eroshi Channel on Telegram'
              rel='noopener noreferrer'
              className='hover:opacity-50 transition-all duration-300'
            >
              <TelegramIcon width='30' height='30' />
            </a>
            <a
              href='https://twitter.com/andrecrjr'
              target='_blank'
              title='My Twitter'
              rel='noopener noreferrer'
              className='ml-3 hover:opacity-50 transition-all duration-300'
            >
              <TwitterIcon width='50' height='30' fill='blue' />
            </a>
          </li>
        </ul>
      </header>
      <main className='mt-16 sm:mt-20 mx-4'>{children}</main>
      <footer className='px-2 pt-5'>
        <p className='text-xs text-white mb-4'>
          I'm not a creator of the Blenderizer smart contract, the smart
          contract creator is:{" "}
          <a href='https://twitter.com/MarcoS3DK' className='underline'>
            MarcosDK
          </a>
          .<br /> this webapp was created just to watch and serve an easy way to
          watch/track this smart contract on WAX Blockchain!
        </p>
        <p className='text-xs text-white'>
          This webapp was created by <strong>Eroshi Front-end Solutions</strong>{" "}
          © {new Date().getFullYear()}
        </p>
      </footer>
    </section>
  );
}

export default Layout;
