import { createContext } from "react";

export const CollectionContext = createContext({
  collection: "freshydroppy",
});
export const MixtureContext = createContext({});
