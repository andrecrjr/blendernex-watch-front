import { useCallback, useState } from "react";
// eslint-disable-next-line
import Worker from "worker-loader!./blenderWax.worker";
const workerBlender = new Worker();

const useBlenderContract = (
  collection = "",
  actionRPC = "INITIAL_PAGE",
  memoCard
) => {
  const [data, setData] = useState([]);
  const getDrops = useCallback(async () => {
    workerBlender.postMessage(
      JSON.stringify({ actionRPC, collection, memoCard })
    );

    workerBlender.addEventListener("message", (e) => {
      setData(e.data);
    });
  }, [collection, actionRPC, memoCard]);
  return [data, getDrops];
};

export default useBlenderContract;
