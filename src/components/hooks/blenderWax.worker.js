/* eslint-disable */
import * as waxjs from "@waxio/waxjs/dist";

const wax = new waxjs.WaxJS("https://wax.greymass.com", null, null, false);

self.addEventListener("message", async ({ data: waxData }) => {
  const { collection, actionRPC, memoCard } = JSON.parse(waxData);
  console.log(collection);
  const data = await wax.rpc.get_table_rows(
    {
      json: true, // Get the response as json
      code: "blenderizerx", // Contract that we target
      scope: "blenderizerx", // Account that owns the data
      table: actionRPC === "INITIAL_PAGE" ? "rambalance" : "blenders",
      reverse: false, // Optional: Get reversed data
      show_payer: false,
      limit: -1,
    },
    {
      blocksBehind: 3,
      expireSeconds: 30,
    }
  );

  if (actionRPC)
    switch (actionRPC) {
      case "INITIAL_PAGE":
        self.postMessage(data.rows);
        break;
      case "BLENDERS_COLLECTION":
        let blendersFromCollection = data.rows.filter(
          (item) => item.collection === collection
        );
        self.postMessage(blendersFromCollection.reverse());
        break;
      case "BLENDER_UNIQUE_MEMO":
        let blenderFromCollectionForUnique = data.rows.filter(
          (item) => item.target === parseInt(memoCard)
        );

        self.postMessage(blenderFromCollectionForUnique);
        break;
      default:
        self.postMessage(data.rows);
        break;
    }
});
