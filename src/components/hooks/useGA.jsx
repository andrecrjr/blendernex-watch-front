import { useState, useCallback } from "react";
import ReactGA from "react-ga";

export const useGA = () => {
  const [initialLoad, setInitialLoad] = useState(false);

  const setPageview = useCallback(() => {
    if (!initialLoad) {
      let numberPage;
      let memoPage;
      if (window.location.search.length > 0) {
        numberPage = new URLSearchParams(window.location.search);
        memoPage = new URLSearchParams(window.location.search);
        if (numberPage.get("page")) {
          numberPage = numberPage.get("page");
        }
        if (memoPage.get("collection")) {
          memoPage = memoPage.get("collection");
        }
      }
      if (memoPage) {
        ReactGA.pageview(
          `${window.location.pathname.replace("memo/", `${memoPage}/memo/`)}`
        );
      } else {
        ReactGA.pageview(
          `${window.location.pathname}${
            (numberPage && parseInt(numberPage) > 0 && `/${numberPage}`) || ""
          }`
        );
      }

      setInitialLoad(true);
    }
  }, [setInitialLoad, initialLoad]);

  return { setPageview, initialLoad };
};
