import { useLocation } from "react-router-dom";

export function useQuery() {
  return new URLSearchParams(useLocation().search);
}

export const usePaginateBlender = (blenders, page = 0, pagination = 5) => {
  let pageBlenders = blenders;
  let totalPages = Math.floor(blenders.length / pagination) + 1;
  if (totalPages >= page) {
    pageBlenders = pageBlenders.slice(
      page * pagination - pagination,
      pagination * page
    );
  } else {
    pageBlenders = [];
  }

  return { totalPages, pageBlenders, perPage: pagination };
};
