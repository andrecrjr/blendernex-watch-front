import React from "react";
import { useHistory } from "react-router-dom";

export default function PaginationControls({ pageNumber, totalPages }) {
  const history = useHistory();

  const controlPage = (sum) => {
    sum
      ? history.push(`?page=${parseInt(pageNumber) + 1}`)
      : history.push(`?page=${parseInt(pageNumber) - 1}`);
    window.scrollTo(0, 0);
    window.location.reload();
  };
  return (
    <section className='flex justify-between pt-5 text-xs md:text-sm'>
      {parseInt(pageNumber) > 1 && (
        <button className='text-white' onClick={(e) => controlPage(false)}>
          ↩ Previous page
        </button>
      )}
      {parseInt(pageNumber) > 0 &&
        totalPages > parseInt(pageNumber) &&
        parseInt(totalPages) !== 1 &&
        totalPages !== parseInt(pageNumber) && (
          <button
            className={`text-white`}
            style={{ flexGrow: pageNumber === "1" && 1 }}
            onClick={(e) => controlPage(true)}
          >
            Next page ↪
          </button>
        )}
    </section>
  );
}
