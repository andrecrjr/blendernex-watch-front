import React, { useReducer } from "react";
import { Card } from "../Card";
import { MixtureContext } from "../contexts";

const MixtureReducer = (state, action) => {
  switch (action.type) {
    case "GET_MIXTURE_TARGET":
      return {
        ...state,
        ...{
          ...action.payload,
          card_done:
            parseInt(action.payload.issued_supply) !==
            parseInt(action.payload.max_supply),
          is_infinite_supply: parseInt(action.payload.issued_supply) === 0,
        },
      };
    case "GET_BORDER_TARGET_COLOR":
      return {
        ...state,
        ...{ target_color: action.color },
      };
    default:
      return state;
  }
};

export const Mixture = ({ pack }) => {
  const [mixtureState, dispatchMixtureReducer] = useReducer(MixtureReducer, {});

  return (
    <MixtureContext.Provider value={{ mixtureState, dispatchMixtureReducer }}>
      <section
        className='flex flex-col itens-center pt-10 pb-10  border-b-2'
        style={{
          borderBottomColor: `${
            Object.keys(mixtureState).length > 0
              ? mixtureState.target_color
              : "white"
          }`,
        }}
      >
        {Object.keys(mixtureState).length > 0 &&
          (mixtureState.card_done || mixtureState.is_infinite_supply) && (
            <div className='text-white text-center md:text-xl pb-3'>
              Send these cards with memo: <strong>{pack.item.target}</strong> to
              blenderizerx and get a new one!
            </div>
          )}
        <section
          className='flex flex-col sm:flex-row sm:grid sm:justify-evenly md:justify-center sm:items-center'
          id={pack.item.target}
        >
          <div className='flex overflow-auto col-end-1'>
            {pack.item.mixture.map((id, index) => (
              <section className='flex' key={index}>
                <Card
                  id={id}
                  target={false}
                  index={index}
                  collection={pack.blendercollection}
                />
                {pack.item.mixture.length - 1 === index ? (
                  <p className='flex items-center mx-2 text-white text-2xl'>
                    =
                  </p>
                ) : (
                  <span className='flex items-center text-2xl text-white mx-2'>
                    +
                  </span>
                )}
              </section>
            ))}
          </div>
          {pack.item.target && (
            <Card
              id={false}
              target={pack.item.target}
              collection={pack.blendercollection}
            />
          )}
        </section>
      </section>
    </MixtureContext.Provider>
  );
};
