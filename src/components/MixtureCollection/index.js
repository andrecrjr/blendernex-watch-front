import React, { useEffect } from "react";
import useBlenderContract from "../hooks/useBlenderContract";
import { Mixture } from "./Mixture";
import { useParams } from "react-router-dom";
import { useQuery, usePaginateBlender } from "../Pagination";
import PaginationControls from "./PaginationControl";
import { useGA } from "../../components/hooks/useGA";

function MainMixture() {
  const { blendercollection } = useParams();
  const { setPageview } = useGA();

  let pageNumber = useQuery().get("page");

  if (pageNumber === null) {
    pageNumber = "1";
  }
  const [blenders, getBlenders] = useBlenderContract(
    blendercollection,
    "BLENDERS_COLLECTION"
  );
  const { totalPages, pageBlenders } = usePaginateBlender(
    blenders,
    parseInt(pageNumber)
  );

  useEffect(() => {
    getBlenders();
  }, [getBlenders]);

  useEffect(() => {
    setPageview();
  }, [setPageview]);

  return (
    <>
      <h1 className='text-white pt-8 text-center text-lg font-bold'>
        {blendercollection} - Page {pageNumber}
      </h1>
      {pageBlenders.length > 0 ? (
        <div className='mx-4 md:mx-28 flex flex-col sm:justify-evenly'>
          {pageBlenders.map((item, index) => (
            <div className='contents' key={index}>
              <Mixture pack={{ item, blendercollection }} />
            </div>
          ))}
          <PaginationControls totalPages={totalPages} pageNumber={pageNumber} />
        </div>
      ) : (
        <div className='text-white text-center py-20'>
          {pageNumber > totalPages
            ? "Hey you came so long!"
            : "This collection not exist, please try another one!"}
        </div>
      )}
    </>
  );
}

export default MainMixture;
