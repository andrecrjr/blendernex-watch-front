import React, { useEffect } from "react";
import useBlenderContract from "../hooks/useBlenderContract";
import { useGA } from "../hooks/useGA";
import { Mixture } from "./Mixture";
import { useHistory, useParams } from "react-router";

export const MemoMixture = () => {
  const { memoCard } = useParams();
  const { setPageview } = useGA();

  const history = useHistory();
  const [memoBlender, getUniqueBlender] = useBlenderContract(
    "",
    "BLENDER_UNIQUE_MEMO",
    memoCard
  );
  useEffect(() => {
    getUniqueBlender();
  }, [getUniqueBlender]);

  useEffect(() => {
    setPageview();
  }, [setPageview]);
  if (memoCard) {
    return (
      <section>
        <h1 className='text-xl text-center text-white'>
          Collection: {memoBlender.length > 0 && memoBlender[0].collection}
        </h1>
        {memoBlender.length > 0 ? (
          <div className='mx-4 md:mx-28 flex flex-col sm:justify-evenly'>
            {memoBlender.map((item, index) => (
              <div className='contents' key={index}>
                <Mixture pack={{ item, blendercollection: item.collection }} />
              </div>
            ))}
          </div>
        ) : (
          <p>This card don't exist!</p>
        )}
      </section>
    );
  }
  return history.push("/");
};
